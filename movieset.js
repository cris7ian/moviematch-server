var _ = require("underscore");
var async = require("async");

var tomatoes = require('tomatoes');
var movies = tomatoes('aptsyvvxvfu8qb33keh6hduw');  // API Key

var MDB_API_KEY = '6931de2906a99776822e6352bddb2475';
var mdb = require('moviedb')(MDB_API_KEY);

var poster_url = "";
var poster_url_original = "";
mdb.configuration({}, function(err, res){
    console.log("Configuration from Movie DB loaded with poster sizes of "+res.images.logo_sizes[4]);
    poster_url = res.images.base_url+res.images.logo_sizes[4];
    poster_url_original = res.images.base_url+res.images.logo_sizes[5];
})

/**
 * Eliminates the duplicates in a given array through the movie title.
 * @param arr the array of movies.
 * @returns {Array}
 */
exports.deleteDuplicates = function (arr) {
    var movies = [];
    var titles = [];
    _.each(arr, function (movie) {
        if (!_.contains(titles, movie.title)) {
            movies.push(movie);
            titles.push(movie.title); //to keep track of the movies we already have.
        }
    });
    return movies;
}

/**
 * Intersects the duplicates in a given array through the movie title.
 * @param arr1 first array.
 * @param arr2 second array of movies.
 * @returns {Array}
 */
function intersect(arr1, arr2) {
    var intersect = [];
    var allMovies = _.union(arr1, arr2);
    //we need the movie to be contained in all of the people's work.
    _.each(allMovies, function (movie) {
        if (!_.isEmpty(_.where(arr1, {title: movie.title}))
            && !_.isEmpty(_.where(arr2, {title: movie.title}))
            && _.isEmpty(_.where(intersect, {title: movie.title}))) {
            intersect.push(movie);
        }
    });
    return intersect;
}

/**
 * intersects and formats the information.
 */
exports.createResults = function (res, movies, useBigPosters) {
    var arr = null;
    for (var actor in movies) {
        if (_.isNull(arr)) { //initialization.
            arr = movies[actor];
        } else {
            arr = intersect(arr, movies[actor]); //only the movies they share.
        }
    }
    var answer = ""; //we just use this variable to print a friendly log.
    var responseJson = {}; //the final response.
    responseJson.moviesTheyWorkedIn = []
    responseJson.status = 1; //so far so good.
    _.each(arr, function (movie) {
        var info = {};
        answer += "Movie: " + movie.title + " (" + _(movie.release_date).strLeft('-') + ")"; //we need only the year when it was released.
        info.movie = movie.title;
        if(movie.poster_path) {
            info.poster = (useBigPosters ? poster_url_original : poster_url)+movie.poster_path;
        }
        info.year = _(movie.release_date).strLeft('-');
        info.people = [];
        for (var actor in movies) {
            var actorJob = _.findWhere(movies[actor], {title: movie.title});
            if (_.isUndefined(actorJob.job)) { //if he was an actor.
                answer += "\n\t" + actor + " was " + actorJob.character;
                info.people.push({name: actor, character: actorJob.character});
            } else { //if he was part of the crew.
                answer += "\n\t" + actor + " worked as a " + actorJob.job;
                info.people.push({name: actor, job: actorJob.job});
            }
        }
        responseJson.moviesTheyWorkedIn.push(info);
        answer += "\n";
    });

    //we now look for the imdb links in Google.
    async.map(responseJson.moviesTheyWorkedIn, getTomatoLink, function(err, results){
        if(err) {
            return;
        }
        results = _.sortBy( results, function(movieSorted){ return parseInt(movieSorted.year); }).reverse();
        responseJson.moviesTheyWorkedIn = results;
        console.log(answer);
        res.json(responseJson);
    })
}

/**
 * Gets the Rotten Tomatoes links of the movie.
 * @param info the info object to extend. We'll use the info.movie attr to query Google.
 * @param callback A callback to async. function(err, transformed).
 */
function getTomatoLink(info, callback) {
    movies.search(info.movie, function(err, links) {
        if (err) {
            console.error(err);
        } else if(_.isEmpty(links)){ //this should never happend. I mean, they're actors!
            console.log("results empty");
        } else {
            var rating = 0; //we use the rating to see if this is the best rated movie with this name.
            _.each(links, function(link){
                if(link.title==info.movie && link.ratings.audience_score >= rating){
                    rating = link.ratings.audience_score;
                    info.link = link.links.alternate; //we'll only need the first link.
                }
            })
        }
        callback(null, info); //we go back to the previous context with this callback.
    });
}
